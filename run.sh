#!/bin/bash

if [[ "$1" == "clean" ]]; then
    ./clean.sh
fi
if [[ "$1" == "compile" ]]; then
    ./clean.sh &&
    xelatex -halt-on-error master-thesis &&
    echo "==========================================================" &&
    biber   master-thesis &&
    echo "==========================================================" &&
    xelatex -halt-on-error master-thesis
fi
